import { Bar } from "../lib";

function App() {
  return (
    <div className="App">
      <Bar />
    </div>
  );
}

export default App;
