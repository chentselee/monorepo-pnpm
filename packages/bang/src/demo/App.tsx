import { Bang } from "../lib";

function App() {
  return (
    <div className="App">
      <Bang />
    </div>
  );
}

export default App;
