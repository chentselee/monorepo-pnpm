import { Foo } from "../lib";

function App() {
  return (
    <div className="App">
      <Foo />
    </div>
  );
}

export default App;
