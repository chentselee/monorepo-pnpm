import { Foo } from "@mylib/foo";
import { Bar } from "@mylib/bar";

export const Baz = () => {
  return (
    <div>
      baz depends on <Foo /> and <Bar />
    </div>
  );
};
