import { Baz } from "../lib";

function App() {
  return (
    <div className="App">
      <Baz />
    </div>
  );
}

export default App;
