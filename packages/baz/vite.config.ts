import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import * as path from "path";

const isDevelopment = process.env.NODE_ENV === "development";

export default defineConfig({
  build: {
    lib: {
      entry: path.resolve(__dirname, "src", "lib", "index.tsx"),
      name: "baz",
    },
    rollupOptions: {
      external: [
        "react",
        "@mylib/foo",
        "@mylib/bar",
        !isDevelopment && "react/jsx-runtime",
      ],
      output: {
        globals: {
          react: "React",
          "@mylib/foo": "foo",
          "@mylib/bar": "bar",
          "react/jsx-runtime": "jsxRuntime",
        },
      },
    },
  },
  plugins: [react()],
});
