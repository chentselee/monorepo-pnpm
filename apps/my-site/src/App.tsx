import { Foo } from "@mylib/foo";
import { Bar } from "@mylib/bar";
import { Baz } from "@mylib/baz";
import { Bang } from "@mylib/bang";

function App() {
  return (
    <div className="App">
      <Foo />
      <Bar />
      <Baz />
      <Bang />
    </div>
  );
}

export default App;
